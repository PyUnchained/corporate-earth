from django.apps import AppConfig


class WebEarthConfig(AppConfig):
    name = 'web_earth'
